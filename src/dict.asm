%include "src/lib.inc"

%define POINTER_SIZE 8

section .text
global find_word
;rdi - key
;rsi - pointer to the beginning of the dictionary
find_word:
.loop:
    push    rsi
    push    rdi
    add     rsi, POINTER_SIZE
    call    string_equals
    pop     rdi
    pop     rsi
    cmp     rax, 1
    je      .end
    cmp     qword[rsi], 0
    je     .not_found
    mov     rsi, [rsi]
    jmp     .loop
.not_found:
    xor     rsi, rsi
.end:
    mov     rax, rsi
    ret
