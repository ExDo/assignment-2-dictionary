%include "src/colon.inc"
%include "src/words.inc"
%include "src/lib.inc"
%include "src/dict.inc"

section .rodata
too_long_error: db "This key is too long. Maximum length is 255 characters", `\n`, 0
not_found:      db "Element with this key was not found", `\n`, 0

section .bss
buffer: resb 256

section .text
global _start

print_error:
    mov     rdx, rsi
    mov     rsi, rdi
    mov     rax, 1      ;write
    mov     rdi, 2      ;stderr
    syscall
    ret

print_element:
    mov     rsi, FIRST
    call    find_word   
    cmp     rax, 0
    je      .end

    add     rax, 8
    mov     rdi, rax
    push    rdi
    call    string_length
    pop     rdi
    add     rdi, rax
    inc     rdi
    call    print_string
    call    print_newline
    mov     rax, 1
.end:
    ret

_start:
    mov     rdi, buffer
    mov     rsi, 256
    call    read_line             ; Не был уверен, какую функцию использовать. read_word или новую read_line.
    cmp     rax, 0
    jne     .printing
    
    mov     rdi, too_long_error
    push    rdi
    call    string_length
    pop     rdi
    mov     rsi, rax
    call    print_error
    call    exit
.printing:
    mov     rdi, rax
    call    print_element
    cmp     rax, 0
    jne     .end

    mov     rdi, not_found
    push    rdi
    call string_length
    pop     rdi
    mov     rsi, rax
    call    print_error
.end:
    call    exit
