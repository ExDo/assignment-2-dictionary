%define FIRST 0
%macro colon 2
    %2: dq FIRST
    db %1, 0
    %define FIRST %2
%endmacro