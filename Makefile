COMPILER=nasm
FLAGS = -g -felf64
SRC_PATH = ./src
BUILD_PATH = ./build
APPLICATION_NAME = FindElement

SOURCES = $(wildcard $(SRC_PATH)/*.asm)
OBJECTS = $(patsubst $(SRC_PATH)/%.asm, $(BUILD_PATH)/%.o, $(SOURCES))
	
$(APPLICATION_NAME) : $(OBJECTS)
	ld $^ -o $(BUILD_PATH)/$(APPLICATION_NAME)

$(BUILD_PATH)/%.o : $(SRC_PATH)/%.asm $(SRC_PATH)/*.inc $(BUILD_PATH)
	$(COMPILER) $(FLAGS) $< -o $@ 

$(BUILD_PATH) : 
	mkdir $(BUILD_PATH)

clean :
	rm -r $(BUILD_PATH)

.PHONY: clean

